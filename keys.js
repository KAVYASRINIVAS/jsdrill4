function findKeys(obj){
    if(!obj){
        return [];
    }
    
    let key=[];
    for(let i in obj){
        key.push(i);
    }
    return key;
}

module.exports = findKeys;

/*
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
console.log(findKeys(testObject));
console.log(findKeys({}));
*/