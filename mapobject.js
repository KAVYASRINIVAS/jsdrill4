function mappingObject(obj,mapObjFunc){
    let result=[];
    if(!obj || !mapObjFunc){
        return [];
    }
    for(let item in obj){
        result[item]=mapObjFunc(obj[item]);
    }
    return result;
}

module.exports = mappingObject;