function findpairs(obj){
    if(!obj){
        return [];
    }
    let pair=[];
    
    for(let i in obj){
        let temp=[];
        temp.push(i);
        temp.push(obj[i]);
        pair.push(temp);
    }

    return pair;
}

module.exports = findpairs;

/*
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
console.log(findpairs(testObject));
console.log(findpairs());
*/