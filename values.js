function findValues(obj){
    if(!obj){
        return [];
    }
    let value=[];
    for(let i in obj){
        value.push(obj[i]);
    }
    return value;
}

module.exports = findValues;

/*
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
console.log(findValues(testObject));
console.log(findValues({}));
*/