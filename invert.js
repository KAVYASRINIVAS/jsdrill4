function invert(obj){
    let result={}
    for(let key in obj){
       let val=obj[key];
       result[val]=result[val] || [];
       result[val].push(key);
    }
    return result;
}

module.exports = invert;