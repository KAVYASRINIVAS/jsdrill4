function defaultFunction(obj,defaultPorperty){
    if(!obj || !defaultPorperty){
        return {};
    }

    let finalObject={};

    for(let key in obj){
        if(!(obj[key])){
            finalObject[key]=defaultPorperty[key];
        }else{
            finalObject[key]=obj[key];
        }
    }
    return finalObject;
}

module.exports = defaultFunction;

/*
let defaultLocation={location : 'Bangalore'}
const testObject = { name: 'Bruce Wayne', age: 36, location: ''};
console.log(defaultFunction(testObject,defaultLocation));
console.log(defaultFunction(testObject));
console.log(defaultFunction(defaultLocation));
*/