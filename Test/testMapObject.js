const testMapObject = require('../mapobject.js');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' } 

function mapObj(val){
    if(typeof val === 'number'){
        return val+5;
    }else if(typeof val === 'string'){
        return val.toUpperCase();
    }else{
        return val;
    }
        

}

console.log(testMapObject(testObject,mapObj));

console.log(testMapObject(testObject));
console.log(testMapObject({},mapObj));